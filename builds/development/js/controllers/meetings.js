myApp.controller('MeetingsController', ['$scope', '$rootScope', '$firebaseArray', 'FIREBASE_URL', function($scope, $rootScope, $firebaseArray, FIREBASE_URL) {
    console.log($rootScope.currentUser);
    var ref = new Firebase(FIREBASE_URL + '/users/' + $rootScope.currentUser.$id + '/meetings');
    var meetingsInfo = $firebaseArray(ref);
    meetingsInfo.$loaded().then(function(data) {
        $scope.meetings = data;
    });
    
    $scope.addMeeting = function() {
        meetingsInfo.$add({
            name: $scope.meetingname,
            date: Firebase.ServerValue.TIMESTAMP
        }).then(function() {
            $scope.meetingname='';
        });
    };//add meeting
    
    $scope.deleteMeeting = function(key) {
        meetingsInfo.$remove(key).then(function (ref) {
            alert('Deleted');
        });
    }; //delete meeting
    
}]);//MeetingsController


                                                                         
                                                                         