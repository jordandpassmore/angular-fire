myApp.controller('RegistrationController' ,['$scope','$location', '$firebaseAuth', 'Authentication', function($scope, $location, $firebaseAuth, Authentication) {
    
    var url = 'https://attendancejdpapp.firebaseio.com/';
    var ref = new Firebase(url);
    var auth = $firebaseAuth(ref);
    
    $scope.login = function () {
       Authentication.login($scope.user)
       .then(function(authData) {
           $location.path('/meetings');
       })
       .catch(function(error) {
           $scope.message = error.message;
       });
    };//login

    $scope.register = function () {
       Authentication.register($scope.user)
       .then(function(user) {
           Authentication.login($scope.user);
           $location.path('/meetings');
       })
       .catch(function(error) {
           $scope.message = error.message;
       });
    }//login
}]);
    