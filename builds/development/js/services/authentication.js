myApp.factory('Authentication', ['$rootScope', '$firebaseAuth', '$routeParams', '$location', '$firebaseObject', 'FIREBASE_URL', function($rootScope, $firebaseAuth, $routeParams, $location, $firebaseObject, FIREBASE_URL) {
    
    var ref = new Firebase(FIREBASE_URL);
    var auth = $firebaseAuth(ref);
    auth.$onAuth(function(authUser) {
        //alert('i got called');
        if (authUser) {
            //alert('i should be setting $rootScope.currentUser now');
            var ref = new Firebase(FIREBASE_URL + '/users/' + authUser.uid);
            var user = $firebaseObject(ref);
            console.log(user);
            $rootScope.currentUser = user;
        } else {
            $rootScope.currentUser = '';
        }
    });
    var myObject = {
        login: function(user) {
            return auth.$authWithPassword({
            email: user.email,
            password: user.password
            });
        }, //login
         logout: function(user) {
            return auth.$unauth();
        },
        register: function(user) {
            return auth.$createUser({
            email: user.email,
            password: user.password
            }).then(function(regUser) {
                var ref = new Firebase(FIREBASE_URL + 'users/' + regUser.uid);
                //var firebaseUsers = $firebase(ref);
                
                var userInfo = {
                    date: Firebase.ServerValue.TIMESTAMP,
                    regUser: regUser.uid,
                    firstname: user.firstname,
                    lastname: user.lastname,
                    email: user.email
                }; //user info
                
                ref.set(userInfo);
            });
        }, //register
        requireAuth: function() {
            return auth.$requireAuth();
        },
        waitForAuth: function() {
            return auth.$waitforAuth();
        },
    
    }; //myObject
    return myObject;
                                     
 }]);